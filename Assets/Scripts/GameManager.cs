﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public RectTransform canvasInstance;
    public RectTransform scorePrefab;
    public int startScore = 40;
    public Text topScoreText;
    public Text botScoreText;
    public Text leftScoreText;
    public Text rightScoreText;

    public TopShipScriptIA topShip;
    public LeftShipScriptIA leftShip;
    public RightShipScriptIA rightShip;
    public ShipScript botShip;

    public GameObject topBarrier;
    public GameObject leftBarrier;
    public GameObject rightBarrier;

    public GameObject victoryMenu;
    public GameObject defeatMenu;

    private int scoreTop;
    private int scoreBot;
    private int scoreLeft;
    private int scoreRight;

    // Use this for initialization
    void Start ()
    {
        scoreTop = startScore;
        scoreBot = startScore;
        scoreRight = startScore;
        scoreLeft = startScore;
    }
	
    RectTransform CreateScore(float player)
    {
        player++;
        Vector2 screenSize = canvasInstance.sizeDelta;
        Vector2 scoreSize = new Vector2(screenSize.x / 4f, screenSize.y / 6f);

        RectTransform scoreContainer = Instantiate(scorePrefab);
        scoreContainer.SetParent(canvasInstance, false);
        scoreContainer.sizeDelta = scoreSize;
        scoreContainer.anchoredPosition3D = new Vector3((scoreSize.x*player) - (scoreSize.x/2f),scoreSize.y/-2f,0f);
        RectTransform imageInstance = scoreContainer.GetComponentInChildren<Image>().gameObject.GetComponent<RectTransform>();
        RectTransform textInstance = scoreContainer.GetComponentInChildren<Text>().gameObject.GetComponent<RectTransform>();

        imageInstance.sizeDelta = Vector2.one * scoreSize.y;
        imageInstance.anchoredPosition3D = new Vector3(imageInstance.sizeDelta.x / -2f, 0f, 0f);

        textInstance.sizeDelta = new Vector2(scoreSize.x - scoreSize.y, scoreSize.y);
        textInstance.anchoredPosition3D = new Vector3(textInstance.sizeDelta.x / 2f, 0f, 0f);


        return scoreContainer;
    }

    void PlayerVictory()
    {
        Time.timeScale = 0f;
        victoryMenu.SetActive(true);
    }

    void PlayerDefeat()
    {
        Time.timeScale = 0f;
        defeatMenu.SetActive(true);
    }

    public void RemovePoint(ShipScript.Position pos)
    {
        switch (pos)
        {
            case ShipScript.Position.Up:
                scoreTop--;
                scoreTop = Mathf.Clamp(scoreTop, 0, startScore);
                topScoreText.text = scoreTop.ToString();
                if(scoreTop == 0)
                {
                    topShip.kill();
                    topBarrier.SetActive(true);
                    if (scoreLeft == 0 && scoreRight == 0)
                        PlayerVictory();
                }
                break;
            case ShipScript.Position.Down:
                scoreBot--;
                scoreBot = Mathf.Clamp(scoreBot, 0, startScore);
                botScoreText.text = scoreBot.ToString();
                if (scoreBot == 0)
                {
                    PlayerDefeat();
                }
                break;
            case ShipScript.Position.Left:
                scoreLeft--;
                scoreLeft = Mathf.Clamp(scoreLeft, 0, startScore);
                leftScoreText.text = scoreLeft.ToString();
                if (scoreLeft == 0)
                {
                    leftShip.kill();
                    leftBarrier.SetActive(true);
                    if (scoreTop == 0 && scoreRight == 0)
                        PlayerVictory();
                }
                break;
            case ShipScript.Position.Right:
                scoreRight--;
                scoreRight = Mathf.Clamp(scoreRight, 0, startScore);
                rightScoreText.text = scoreRight.ToString();
                if (scoreRight == 0)
                {
                    rightShip.kill();
                    rightBarrier.SetActive(true);
                    if (scoreLeft == 0 && scoreTop == 0)
                        PlayerVictory();
                }
                break;
            default:
                break;
        }
    }
}
