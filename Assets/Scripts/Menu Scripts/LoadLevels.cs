﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevels : MonoBehaviour {

	
    public void LoadMainLevel()
    {
        SceneManager.LoadScene("gameScene");
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("menuScene");
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
    }
}
