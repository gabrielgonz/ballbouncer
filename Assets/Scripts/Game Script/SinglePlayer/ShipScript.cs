﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipScript : MonoBehaviour {

    public enum Position
    {
        Up,
        Down,
        Left,
        Right
    }

    public float radius = 10f;
    public Position position = Position.Down;
    public bool moveLeft = false;
    public bool moveRight = false;

    public void Kill()
    {
        GetComponent<ShipScript>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
        StartCoroutine(KillChar());
    }

    IEnumerator KillChar()
    {
        Vector3 startScale = transform.localScale;
        Vector3 endScale = Vector3.zero;
        float lerpTime = 0.5f;
        float currentLerpTime = 0f;
        bool reduce = true;

        while (reduce)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
                reduce = false;
            }

            //lerp!
            float perc = currentLerpTime / lerpTime;
            transform.localScale = Vector3.Lerp(startScale, endScale, perc);
            yield return 0;
        }

        yield return 0;
        Destroy(gameObject);
    }

    public void StartMoveLeft()
    {
        moveLeft = true;
    }

    public void StopMoveLeft()
    {
        moveLeft = false;
    }

    public void StartMoveRight()
    {
        moveRight = true;
    }

    public void StopMoveRight()
    {
        moveRight = false;
    }

    // Update is called once per frame
    void Update ()
    {
        float maxX = radius * Mathf.Sin(30 * Mathf.Deg2Rad);
        float maxY = radius * Mathf.Sin((180-90-30) * Mathf.Deg2Rad); ;

        if (moveLeft && !moveRight || Input.GetAxisRaw("Horizontal") < 0)
            transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * -1 * -50f);

        if (!moveLeft && moveRight || Input.GetAxisRaw("Horizontal") > 0)
            transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * 1 * -50f);

        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, -maxX, maxX);
        pos.z = Mathf.Clamp(pos.z, -radius, -maxY);
        transform.position = pos;
    }
}
