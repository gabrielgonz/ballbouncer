﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopShipScriptIA : MonoBehaviour
{
    public DetectionSphere detectionSphere;
    public float radius = 4.5f;
    public float speed = 50f;
    public bool moveLeft = false;
    public bool moveRight = false;

    public void kill()
    {
        GetComponent<TopShipScriptIA>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
        StartCoroutine(killChar());
    }

    IEnumerator killChar()
    {
        Vector3 startScale = transform.localScale;
        Vector3 endScale = Vector3.zero;
        float lerpTime = 0.5f;
        float currentLerpTime = 0f;
        bool reduce = true;

        while(reduce)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
                reduce = false;
            }

            //lerp!
            float perc = currentLerpTime / lerpTime;
            transform.localScale = Vector3.Lerp(startScale, endScale, perc);
            yield return 0;
        }

        yield return 0;
        Destroy(gameObject);
    }

    void Update()
    {

        if (transform.position.x - 0.1f > detectionSphere.collitionPoint.x)
            moveRight = true;
        else
            moveRight = false;

        if (transform.position.x + 0.1f < detectionSphere.collitionPoint.x)
            moveLeft = true;
        else
            moveLeft = false;

        float maxX = radius * Mathf.Sin(30 * Mathf.Deg2Rad);
        float maxY = radius * Mathf.Sin((180 - 90 - 30) * Mathf.Deg2Rad); ;

        if (moveLeft && !moveRight)
            transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * speed);

        if (!moveLeft && moveRight)
            transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * -1 * speed);

        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, -maxX, maxX);
        pos.z = Mathf.Clamp(pos.z, maxY, radius);
        transform.position = pos;
    }

}
