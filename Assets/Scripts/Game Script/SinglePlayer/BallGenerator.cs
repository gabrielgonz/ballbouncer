﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour {

    public GameObject ballPrefab;

    private float timer = 0f;
    private float waitTime = 2f;

	// Use this for initialization
	void Start ()
    {
        waitTime = Random.Range(5f, 8f);
	}
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;

        if(timer > waitTime)
        {
            waitTime = Random.Range(5f, 8f);
            timer = 0f;
            BallScript rigidBall = Instantiate(ballPrefab,transform.position,Quaternion.identity).GetComponent<BallScript>();
            rigidBall.direction = -((transform.position)).normalized + new Vector3(Random.Range(-0.2f,0.2f),0f, Random.Range(-0.2f, 0.2f));
        }
	}
}
