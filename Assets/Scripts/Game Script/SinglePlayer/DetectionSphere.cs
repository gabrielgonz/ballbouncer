﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class DetectionSphere : MonoBehaviour {

    public List<Transform> detectionList;
    public float sphereRadius = 3f;
    public Vector3 collitionPoint = Vector3.zero;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
            if (!alreadyThere(other.transform))
                detectionList.Add(other.transform);
    }

    bool alreadyThere(Transform obj)
    {
        for(int i = 0;i<detectionList.Count;i++)
        {
            if (detectionList[i] == obj)
                return true;
        }
        return false;
    }

    void checkIfNull()
    {
        for (int i = 0; i < detectionList.Count; i++)
        {
            if (detectionList[i] == null)
                detectionList.RemoveAt(i);
        }
    }

    void sortByDistance()
    {
        for (int i = 0; i < detectionList.Count; i++)
        {
            for (int j = i+1; j < detectionList.Count; j++)
            {
                if(Vector3.Distance(transform.position,detectionList[j].position) < Vector3.Distance(transform.position, detectionList[i].position))
                {
                    Transform saveData = detectionList[i];
                    detectionList[i] = detectionList[j];
                    detectionList[j] = saveData;
                }
            }
        }
    }

    void removeBehind()
    {
        for (int i = detectionList.Count - 1; i >= 0; i--)
        {
            if (Vector3.Distance(Vector3.zero, detectionList[i].position) > 4.5f)
                detectionList.RemoveAt(i);
        }
    }

    bool intersection(out float distance,Vector3 dirVect,Vector3 startPoint)
    {
        Vector3 d = (startPoint+dirVect*5f) - startPoint;
        Vector3 f = startPoint;
        distance = 0f;

        float a = Vector3.Dot(d, d);
        float b = 2 * Vector3.Dot(f, d);
        float c = Vector3.Dot(f, f) - 4.5f * 4.5f;

        float discriminant = b * b - 4 * a * c;
        if (discriminant < 0)
        {
            // no intersection
            return false;
        }
        else
        {
            // ray didn't totally miss sphere,
            // so there is a solution to
            // the equation.

            discriminant = Mathf.Sqrt(discriminant);

            // either solution may be on or off the ray so need to test both
            // t1 is always the smaller value, because BOTH discriminant and
            // a are nonnegative.
            float t1 = (-b - discriminant) / (2 * a);
            float t2 = (-b + discriminant) / (2 * a);

            // 3x HIT cases:
            //          -o->             --|-->  |            |  --|->
            // Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit), 

            // 3x MISS cases:
            //       ->  o                     o ->              | -> |
            // FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)

            if (t1 >= 0 && t1 <= 1)
            {
                // t1 is the intersection, and it's closer than t2
                // (since t1 uses -b - discriminant)
                // Impale, Poke
                return true;
            }

            // here t1 didn't intersect so we are either started
            // inside the sphere or completely past it
            if (t2 >= 0 && t2 <= 1)
            {
                // ExitWound
                distance = t2 * 4.5f;
                return true;
            }

            // no intn: FallShort, Past, CompletelyInside
            return false;
        }
    }

    Vector3 CollitionPoint()
    {
        Vector3 currentPos = detectionList[0].position;
        Vector3 speedDirection = detectionList[0].GetComponent<Rigidbody>().velocity.normalized;
        float dist = 0f;
        intersection(out dist, speedDirection, currentPos);

        return (currentPos + (speedDirection * dist));
    }

    // Use this for initialization
    void Start ()
    {
        SphereCollider collider = GetComponent<SphereCollider>();
        collider.isTrigger = true;
        collider.radius = sphereRadius;
	}
	
	// Update is called once per frame
	void Update ()
    {
        checkIfNull();
        sortByDistance();
        removeBehind();

        if (detectionList.Count != 0)
            collitionPoint = CollitionPoint();
        else
            collitionPoint = Vector3.zero;
	}
}
