﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpUp : MonoBehaviour {

    public float distance;
    public float waitTime;


    private void OnEnable()
    {
        StartCoroutine(goUp());
    }

    IEnumerator goUp()
    {
        if (waitTime != 0)
            new WaitForSeconds(waitTime);

        Vector3 startPos = transform.position;
        Vector3 endPos = startPos;
        endPos.y = distance;
        float lerpTime = 0.5f;
        float currentLerpTime = 0f;
        bool up = true;

        while(up)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
                up = false;
            }

            //lerp!
            float perc = currentLerpTime / lerpTime;
            transform.position = Vector3.Lerp(startPos, endPos, perc);
            yield return 0;
        }

    }

}
