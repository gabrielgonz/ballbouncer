﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCollider : MonoBehaviour {

    public ShipScript.Position position;
    public GameManager managerInstance;

    private void OnTriggerEnter(Collider other)
    {
        managerInstance.RemovePoint(position);
        Destroy(other.gameObject);
    }
}
