﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftShipScriptIA : MonoBehaviour
{
    public DetectionSphere detectionSphere;
    public float radius = 4.5f;
    public float speed = 50f;
    public bool moveLeft = false;
    public bool moveRight = false;

    public void kill()
    {
        GetComponent<LeftShipScriptIA>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
        StartCoroutine(killChar());
    }

    IEnumerator killChar()
    {
        Vector3 startScale = transform.localScale;
        Vector3 endScale = Vector3.zero;
        float lerpTime = 0.5f;
        float currentLerpTime = 0f;
        bool reduce = true;

        while (reduce)
        {
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
                reduce = false;
            }

            //lerp!
            float perc = currentLerpTime / lerpTime;
            transform.localScale = Vector3.Lerp(startScale, endScale, perc);
            yield return 0;
        }

        yield return 0;
        Destroy(gameObject);
    }

    void Update()
    {
        if (transform.position.z+0.1f < detectionSphere.collitionPoint.z)
        {
            moveLeft = true;
        }else
        {
            moveLeft = false;
        }

        if (transform.position.z-0.1f > detectionSphere.collitionPoint.z)
        {
            moveRight = true;
        }else
        {
            moveRight = false;
        }

        float maxY = radius * Mathf.Sin(30 * Mathf.Deg2Rad);
        float maxX = radius * Mathf.Sin((180 - 90 - 30) * Mathf.Deg2Rad);

        if (moveLeft && !moveRight)
            transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * speed);

        if (!moveLeft && moveRight)
            transform.RotateAround(Vector3.zero, Vector3.up, Time.deltaTime * -1 * speed);

        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, -radius, -maxX);
        pos.z = Mathf.Clamp(pos.z, -maxY, maxY);
        transform.position = pos;
    }

}
