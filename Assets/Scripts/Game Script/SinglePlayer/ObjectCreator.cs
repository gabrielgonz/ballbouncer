﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCreator : MonoBehaviour {

    public GameObject objPrefab;
    public float radius = 10f;

    private Vector3[] posList = new Vector3[4];

	// Use this for initialization
	void Start ()
    {
        float posX = radius * Mathf.Sin(45 * Mathf.Deg2Rad);
        float posY = radius * Mathf.Sin(45 * Mathf.Deg2Rad);

        posList[0] = new Vector3(posX, 1f, posY);
        posList[1] = new Vector3(-posX, 1f, posY);
        posList[2] = new Vector3(posX, 1f, -posY);
        posList[3] = new Vector3(-posX, 1f, -posY);

        foreach (Vector3 item in posList)
        {
            Instantiate(objPrefab, item, Quaternion.identity);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
