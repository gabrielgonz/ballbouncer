﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour {

    public float constantSpeed = 10f;
    public Vector3 direction = Vector3.back;
    private Rigidbody rigidbodyComponent;

	// Use this for initialization
	void Start ()
    {
        rigidbodyComponent = GetComponent<Rigidbody>();
        rigidbodyComponent.velocity = direction;
    }

    private void FixedUpdate()
    {
        rigidbodyComponent.velocity = constantSpeed * (rigidbodyComponent.velocity.normalized);
        
    }
}
